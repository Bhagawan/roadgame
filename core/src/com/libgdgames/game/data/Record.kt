package com.libgdgames.game.data

import java.util.Date

class Record() {
    var record: Int = 0
    var time: Date = Date()
    constructor(r: Int, d: Date): this() {
        record = r
        time = d
    }
}
