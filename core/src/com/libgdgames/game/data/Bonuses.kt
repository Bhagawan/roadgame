package com.libgdgames.game.data

enum class Bonuses(val spriteName: String) {
    OIL("oil_puddle"), GAS("gas")
}