package com.libgdgames.game.components

import com.badlogic.ashley.core.Component

data class MoveComponent(var dX: Float, var dY: Float): Component
