package com.libgdgames.game.components

import com.badlogic.ashley.core.Component
import com.libgdgames.game.data.Bonuses

class BonusComponent(val bonus: Bonuses): Component