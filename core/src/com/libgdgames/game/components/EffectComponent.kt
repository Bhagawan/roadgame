package com.libgdgames.game.components

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect

data class EffectComponent(val x: Float, val y: Float, val effect: PooledEffect): Component {

    init {
        effect.setPosition(x, y)
        effect.allowCompletion()
        effect.start()
    }
}