package com.libgdgames.game.screens

import com.badlogic.ashley.core.Engine
import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.utils.ScreenUtils
import com.libgdgames.game.data.Bonuses
import com.libgdgames.game.systems.BackgroundSystem
import com.libgdgames.game.systems.CollisionSystem
import com.libgdgames.game.systems.ControlSystem
import com.libgdgames.game.systems.EffectRenderSystem
import com.libgdgames.game.systems.EntityRemoveSystem
import com.libgdgames.game.systems.MovementSystem
import com.libgdgames.game.systems.RenderSystem
import com.libgdgames.game.systems.SpawnSystem
import com.libgdgames.game.systems.UISystem

class GameScreen(private val game: Game): ScreenAdapter() {
    private val camera = OrthographicCamera()
    private val engine = Engine()

    private val controlSystem = ControlSystem(20)
    private val uiSystem = UISystem(120)

    init {
        camera.setToOrtho(false)

        val backgroundSystem = BackgroundSystem(1)
        val spawnSystem = SpawnSystem(2)
        val movementSystem = MovementSystem(80).apply {
            setInterface(object : MovementSystem.MovementSystemInterface {
                override fun updateSpeed(speed: Float) {
                    //spawnSystem.increaseSpeed(speed)
                    backgroundSystem.setSpeed(speed)
                }
            })
        }
        uiSystem.setInterface(object : UISystem.UIInterface {
            override fun pause() {
                controlSystem.pauseGame()
            }

            override fun resume() {
                controlSystem.resumeGame()
            }

            override fun restart() {
                controlSystem.restartGame()
                spawnSystem.restartGame()
                movementSystem.restart()
            }

            override fun exit() {
                game.screen = MenuScreen(game)
            }

        })

        engine.addSystem(backgroundSystem)
        engine.addSystem(spawnSystem)
        engine.addSystem(movementSystem)
        engine.addSystem(CollisionSystem(90).apply {
            setInterface(object : CollisionSystem.CollisionSystemInterface {
                override fun explosion(x: Float, y: Float) = controlSystem.explosion(x, y)

                override fun applyBonus(bonus: Bonuses) = controlSystem.applyBonus(bonus)
            })
        })
        engine.addSystem(controlSystem)
        engine.addSystem(EntityRemoveSystem(81))
        engine.addSystem(RenderSystem(camera, 100))
        engine.addSystem(EffectRenderSystem(camera,101))
        engine.addSystem(uiSystem)

        controlSystem.setInterface(object : ControlSystem.ControlInterface {
            override fun endGameMenu() {
                uiSystem.end()
            }

            override fun increaseSpeed() {
                movementSystem.increaseSpeed()
            }
        })
    }

    override fun show() {
        val inputAdapter = object : InputAdapter() {

            override fun keyDown(keycode: Int): Boolean = when(keycode) {
                Input.Keys.TAB -> {
                    game.screen = MenuScreen(game)
                    true
                }
                else -> false
            }

            override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
                return true
            }

            override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
                return true
            }

            override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
                controlSystem.touch(screenX.toFloat())
                return true
            }
        }
        val inputMultiplexer = InputMultiplexer()
        inputMultiplexer.addProcessor(uiSystem.getStage())
        inputMultiplexer.addProcessor(inputAdapter)
        Gdx.input.inputProcessor = inputMultiplexer
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0.0f, 0.0f, 0.0f, 1.0f)
        engine.update(delta)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
    }
}