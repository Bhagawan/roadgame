package com.libgdgames.game.screens

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.ScreenUtils
import com.libgdgames.game.RoadGame
import com.libgdgames.game.assets.Assets

class MenuScreen(private val game: Game): ScreenAdapter() {
    private val stage = Stage()

    init {
        createMenu()
    }

    override fun show() {
        val inputAdapter = object : InputAdapter() {

            override fun keyDown(keycode: Int): Boolean = when(keycode) {
                Input.Keys.TAB -> {
                    true
                }
                else -> false
            }
        }
        val inputMultiplexer = InputMultiplexer()
        inputMultiplexer.addProcessor(inputAdapter)
        inputMultiplexer.addProcessor(stage)
        Gdx.input.inputProcessor = inputMultiplexer
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0.0f, 0.0f, 0.0f, 1.0f)
        stage.act(Gdx.graphics.deltaTime.coerceAtMost(RoadGame.PERFECT_DT))
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
        stage.clear()
    }

    override fun dispose() {
        stage.dispose()
    }

    private fun createMenu() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val table = Table()
        table.setFillParent(true)
        table.background = TiledDrawable(skin.getRegion("grass_tile"))
        stage.addActor(table)

        table.pad(20.0f)

        val btnWidth = Gdx.graphics.width / 2.0f
        table.row()

        val logo = Label("Road Game", skin, "logo").apply { setAlignment(Align.center) }
        table.top().center().add(logo).width(Gdx.graphics.width / 1.5f).height(Gdx.graphics.height / 12.0f).align(Align.center)
        table.row()
        table.add().height(Gdx.graphics.height / 12.0f)
        table.row()

        val newGameButton = TextButton("New Game", skin, "menu").apply {
            addListener(object : ClickListener() {
                override fun touchUp(
                    event: InputEvent?,
                    x: Float,
                    y: Float,
                    pointer: Int,
                    button: Int
                ) {
                    game.screen = GameScreen(game)
                }
            })
        }

        table.center().add(newGameButton).size(btnWidth * 1.2f, btnWidth / 4.0f).padBottom(30.0f).padTop(30.0f).center()
        table.row()

        val recordsButton = TextButton("Records", skin, "menu").apply {
            addListener(object : ClickListener() {
                override fun touchUp(
                    event: InputEvent?,
                    x: Float,
                    y: Float,
                    pointer: Int,
                    button: Int
                ) {
                    game.screen = RecordsScreen(game)
                }
            })
        }

        table.center().add(recordsButton).size(btnWidth * 1.1f, btnWidth / 4.0f).center().padBottom(30.0f)
        table.row()

        val image = SpriteDrawable(skin.getSprite("road").apply { rotate(90.0f) })
        table.add(Image(image)).expandY().width(Gdx.graphics.width / 3.0f).fill().padBottom(10.0f).padTop(10.0f)
        table.row()

        val exitButton = TextButton("Exit",skin, "menu").apply { addListener( object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    Gdx.app.exit()
                }
            })
        }
        table.center().add(exitButton).size(btnWidth, btnWidth / 4.0f).center().padBottom(50.0f).expandX().bottom()
        table.row()
    }
}