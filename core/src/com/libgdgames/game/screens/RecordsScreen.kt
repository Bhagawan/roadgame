package com.libgdgames.game.screens

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Container
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.ScreenUtils
import com.libgdgames.game.RoadGame
import com.libgdgames.game.assets.Assets
import com.libgdgames.game.util.Records

class RecordsScreen(private val game: Game): ScreenAdapter() {
    private val stage = Stage()

    init {
        createMenu()
    }

    override fun show() {
        Gdx.input.inputProcessor = stage
    }

    override fun render(delta: Float) {
        ScreenUtils.clear(0.0f, 0.0f, 0.0f, 1.0f)
        stage.act(Gdx.graphics.deltaTime.coerceAtMost(RoadGame.PERFECT_DT))
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height)
    }

    override fun hide() {
        Gdx.input.inputProcessor = null
        stage.clear()
    }

    override fun dispose() {
        stage.dispose()
    }

    private fun createMenu() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val table = Table()
        table.setFillParent(true)
        table.background = TiledDrawable(skin.getRegion("grass_tile"))
        stage.addActor(table)

        table.pad(20.0f)

        val btnExit = ImageButton(SpriteDrawable(skin.getSprite("btn_exit")
            .apply { setSize(Gdx.graphics.width / 10.0f,Gdx.graphics.width / 10.0f) }))
            .apply {
            addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    game.screen = MenuScreen(game)
                }
            })
        }
        table.add(btnExit).expand().top().left().size(Gdx.graphics.width / 5.0f)
        table.row()

        val innerTable = Table()
        innerTable.background(skin.getDrawable("panel"))
        innerTable.pad(10.0f).padTop(Gdx.graphics.height / 25.0f)
        table.center().add(innerTable).size(Gdx.graphics.width / 2.0f, Gdx.graphics.height / 2.0f)

        val header = Label("Records", skin, "Font_60", "Red")
        innerTable.top().center().add(header).expandX()
        innerTable.row().padTop((Gdx.graphics.height / 16.0f))

        val recordsGroup = VerticalGroup()
        val scroll = ScrollPane(recordsGroup)
        scroll.setFlickScroll(true)
        recordsGroup.space(10.0f)

        innerTable.center().add(scroll).expand().top().minHeight(10.0f)

        for(record in Records.getRecords()) {
            val l = Label(record.record.toString(), skin, "Font_60", "White").apply { setAlignment(Align.center)}
            recordsGroup.addActor(Container(l).apply { prefWidth(Gdx.graphics.width / 2.0f - 20.0f)})
        }
        table.row()
        table.add().expand()
    }
}