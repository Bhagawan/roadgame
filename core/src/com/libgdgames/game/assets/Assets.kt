package com.libgdgames.game.assets

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader
import com.badlogic.gdx.graphics.g2d.ParticleEffect
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.scenes.scene2d.ui.Skin

class Assets {
    companion object {
        var assetManager = AssetManager()
        lateinit var explosionPool : ParticleEffectPool

        fun init() {
            assetManager = AssetManager()
            assetManager.load("skin/skin.atlas", TextureAtlas::class.java)
            assetManager.load("skin/skin.json", Skin::class.java)

            val partParameter = ParticleEffectLoader.ParticleEffectParameter()
            partParameter.atlasFile = "skin/skin.atlas"
            assetManager.load("effect_explosion", ParticleEffect::class.java, partParameter)

            while(!assetManager.update()) { println("Loading assets") }

            explosionPool = ParticleEffectPool((assetManager.get("effect_explosion", ParticleEffect::class.java)), 1, 10)
        }

        fun dispose() {
            assetManager.dispose()
        }

    }
}