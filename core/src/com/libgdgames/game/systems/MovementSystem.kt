package com.libgdgames.game.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.libgdgames.game.RoadGame
import com.libgdgames.game.components.MoveComponent
import com.libgdgames.game.components.TransformComponent

class MovementSystem(priority: Int): IteratingSystem(Family.all(MoveComponent::class.java, TransformComponent::class.java).get(), priority) {
    private val mM = ComponentMapper.getFor(MoveComponent::class.java)
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)
    private var movementInterface: MovementSystemInterface? = null

    private var speedD = 1.0f

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        val tC = tM.get(entity)
        val mC = mM.get(entity)
        tC.pos.x += mC.dX * (deltaTime / RoadGame.PERFECT_DT) * speedD
        tC.pos.y += mC.dY * (deltaTime / RoadGame.PERFECT_DT) * speedD
    }

    fun setInterface(i: MovementSystemInterface) {
        movementInterface = i
    }

    fun increaseSpeed() {
        speedD *= 1.01f
        movementInterface?.updateSpeed(speedD)
    }

    fun restart() {
        speedD = 1.0f
        movementInterface?.updateSpeed(speedD)
    }

    interface MovementSystemInterface {
        fun updateSpeed(speed: Float)
    }
}