package com.libgdgames.game.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.libgdgames.game.components.EffectComponent

class EffectRenderSystem(private val camera: Camera, priority: Int):
    IteratingSystem(Family.all(EffectComponent::class.java).get(), priority) {

    private val spriteBath = SpriteBatch()
    private val eM = ComponentMapper.getFor(EffectComponent::class.java)

    override fun update(deltaTime: Float) {
        camera.update()
        spriteBath.projectionMatrix = camera.combined

        spriteBath.begin()
        super.update(deltaTime)
        spriteBath.end()
    }

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        val eC = eM.get(entity)
        if(!eC.effect.isComplete) eC.effect.draw(spriteBath, deltaTime)
        else {
            eC.effect.free()
            engine.removeEntity(entity)
        }
    }
}