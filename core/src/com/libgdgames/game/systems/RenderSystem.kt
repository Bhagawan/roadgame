package com.libgdgames.game.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.Array
import com.libgdgames.game.components.DrawComponent
import com.libgdgames.game.components.TransformComponent
import kotlin.math.sign

class RenderSystem(private val camera: Camera, priority: Int):
    IteratingSystem(Family.all(DrawComponent::class.java, TransformComponent::class.java).get(), priority) {

    private val spriteBath = SpriteBatch()
    private val dM = ComponentMapper.getFor(DrawComponent::class.java)
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)

    private val queue = Array<Entity>()

    private val comparator =
        Comparator<Entity> { p0, p1 -> (tM.get(p0).pos.z - tM.get(p1).pos.z).sign.toInt() }

    override fun update(deltaTime: Float) {
        super.update(deltaTime)
        camera.update()
        spriteBath.projectionMatrix = camera.combined

        queue.sort(comparator)

        var dC: DrawComponent?
        var tC: TransformComponent?

        spriteBath.begin()
        for (entity in queue) {
            dC = dM.get(entity)
            tC = tM.get(entity)

            dC.tiledDrawable?.draw(spriteBath, tC.pos.x, tC.pos.y, tC.width, tC.height)
            dC.textureRegion?.let {
                spriteBath.draw(
                    it, tC.pos.x, tC.pos.y, tC.originX, tC.originY,
                    tC.width, tC.height, tC.scale, tC.scale, tC.angle)
            }
        }
        spriteBath.end()
        queue.clear()
    }

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        entity?.let { queue.add(it) }
    }
}