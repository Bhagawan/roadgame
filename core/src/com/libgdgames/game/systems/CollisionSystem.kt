package com.libgdgames.game.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntityListener
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.libgdgames.game.components.BonusComponent
import com.libgdgames.game.components.CarComponent
import com.libgdgames.game.components.PlayerComponent
import com.libgdgames.game.components.TransformComponent
import com.libgdgames.game.data.Bonuses
import kotlin.math.absoluteValue

class CollisionSystem(priority: Int): IteratingSystem(Family.all(TransformComponent::class.java).one(CarComponent::class.java, BonusComponent::class.java).get(), priority) , EntityListener {
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)
    private val pM = ComponentMapper.getFor(PlayerComponent::class.java)
    private val cM = ComponentMapper.getFor(CarComponent::class.java)
    private val bM = ComponentMapper.getFor(BonusComponent::class.java)
    private var playerTC: TransformComponent? = null

    private var collisionInterface: CollisionSystemInterface? = null

    override fun addedToEngine(engine: Engine?) {
        val players = engine?.getEntitiesFor(Family.all(PlayerComponent::class.java, TransformComponent::class.java).get())
        players?.let {
            if(it.size() > 0) {
                val pTC = tM.get(it.first())
                if(pTC != null) playerTC = pTC
            }
        }
        super.addedToEngine(engine)
        engine?.addEntityListener(this)
    }

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        playerTC?.let {
            val tC = tM.get(entity)
            if(it.pos.y - tC.pos.y in -it.height..tC.height && (it.pos.x - tC.pos.x).absoluteValue in -it.width..tC.width) {
                val cC = cM.get(entity)
                val bC = bM.get(entity)
                if(cC != null) {
                    val x = if(it.pos.x < tC.pos.x) tC.pos.x + (it.pos.x + it.width - tC.pos.x) / 2.0f
                    else it.pos.x + (tC.pos.x + tC.width - it.pos.x) / 2.0f
                    val y = if(it.pos.y < tC.pos.y) tC.pos.y + (it.pos.y + it.height - tC.pos.y) / 2.0f
                    else it.pos.y + (tC.pos.y + tC.height - it.pos.y) / 2.0f
                    collisionInterface?.explosion(x, y)
                } else if(bC != null) {
                    collisionInterface?.applyBonus(bC.bonus)
                    engine.removeEntity(entity)
                }
            }
        }
    }

    override fun entityAdded(entity: Entity?) {
        entity?.let {
            if(pM.get(it) != null) {
                val pTC = tM.get(entity)
                if(pTC != null) playerTC = pTC
            }
        }
    }

    override fun entityRemoved(entity: Entity?) {}

    fun setInterface(i: CollisionSystemInterface) {
        collisionInterface = i
    }

    interface CollisionSystemInterface {
        fun explosion(x: Float, y: Float)
        fun applyBonus(bonus: Bonuses)
    }
}