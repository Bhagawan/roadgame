package com.libgdgames.game.systems

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable
import com.libgdgames.game.RoadGame
import com.libgdgames.game.assets.Assets
import com.libgdgames.game.components.DrawComponent
import com.libgdgames.game.components.TransformComponent

class BackgroundSystem(priority: Int): EntitySystem(priority) {
    private var speed = 5.0f
    private val back = Entity()
    private var tileSize = 1.0f
    private var moveD = 0.0f
    private var roadD = 0.0f
    private val roadStep = Gdx.graphics.height / 560.0f * 80
    private val position = TransformComponent(0.0f,0.0f, 0.0f, Gdx.graphics.width.toFloat(), Gdx.graphics.height + tileSize - moveD)
    private val roadTC = TransformComponent(Gdx.graphics.width  / 4.0f,0.0f, 0.5f, Gdx.graphics.width  / 2.0f, Gdx.graphics.height / 560.0f * 640)

    private var timeD = 0.0f

    override fun addedToEngine(engine: Engine?) {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)

        tileSize = skin.getSprite("grass_tile").height
        position.height = Gdx.graphics.height + tileSize - moveD
        back.add(position)
        back.add(DrawComponent(TiledDrawable(skin.getSprite("grass_tile"))))

        engine?.addEntity(back)

        val road = Entity()
        road.add(roadTC)
        road.add(DrawComponent(skin.getRegion("road")))

        engine?.addEntity(road)
    }

    override fun update(deltaTime: Float) {
        timeD = 0.0f
        moveD = (moveD - speed * (deltaTime / RoadGame.PERFECT_DT)) % tileSize
        roadD = (roadD - speed * (deltaTime / RoadGame.PERFECT_DT)) % roadStep
        position.pos.y = moveD
        roadTC.pos.y = roadD

    }

    fun setSpeed(newSpeed: Float) {
        speed = 5 * newSpeed
    }
}