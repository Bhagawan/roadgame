package com.libgdgames.game.systems

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.ashley.core.Family
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.libgdgames.game.RoadGame
import com.libgdgames.game.assets.Assets
import com.libgdgames.game.components.BonusComponent
import com.libgdgames.game.components.CarComponent
import com.libgdgames.game.components.DrawComponent
import com.libgdgames.game.components.MoveComponent
import com.libgdgames.game.components.TransformComponent
import com.libgdgames.game.data.Bonuses
import kotlin.random.Random

class SpawnSystem(priority: Int):EntitySystem(priority) {
    private val roadSize: Float = Gdx.graphics.width / 2.0f
    private val carWidth = roadSize * (56.0f / 300.0f)
    private val roadLines = listOf(
        (Gdx.graphics.width - roadSize) / 2.0f + roadSize * (19.0f / 300.0f),
        (Gdx.graphics.width - roadSize) / 2.0f + roadSize * (83.0f / 300.0f),
        (Gdx.graphics.width - roadSize) / 2.0f + roadSize * (164.0f / 300.0f),
        (Gdx.graphics.width - roadSize) / 2.0f + roadSize * (227.0f / 300.0f)
    )
    private var speed = START_SPEED
    private var spawnSpeed = 10.0f

    private var treeSpawnTimer = 0.0f
    private var carSpawnTimer = 0.0f
    private var bonusSpawnTimer = 0.0f

    companion object {
        const val TREE_SPAWN_THRESHOLD = 5.0f
        const val CAR_SPAWN_THRESHOLD = 75.0f
        const val BONUS_SPAWN_THRESHOLD = 35.0f
        const val START_SPEED = 5.0f
    }

    override fun update(deltaTime: Float) {
        treeSpawnTimer += speed / 60.0f * spawnSpeed * (deltaTime / RoadGame.PERFECT_DT)
        carSpawnTimer += speed / 60.0f * spawnSpeed * (deltaTime / RoadGame.PERFECT_DT)
        bonusSpawnTimer += speed / 60.0f * spawnSpeed * (deltaTime / RoadGame.PERFECT_DT)
        if(treeSpawnTimer >= TREE_SPAWN_THRESHOLD) {
            treeSpawnTimer = 0.0f
            spawnTree()
        }
        if(carSpawnTimer >= CAR_SPAWN_THRESHOLD) {
            carSpawnTimer = 0.0f
            spawnCar()
        }
        if(bonusSpawnTimer >= BONUS_SPAWN_THRESHOLD) {
            bonusSpawnTimer = 0.0f
            spawnBonus()
        }

    }

    fun restartGame() {
        engine.removeAllEntities(Family.one(CarComponent::class.java, BonusComponent::class.java).get())
    }

    private fun spawnTree() {
        val treeSize = roadSize / 3.0f
        val tree = Entity()
        val treeName = listOf(
            "tree_1",
            "tree_2",
            "tree_3"
        ).random()
        tree.add(DrawComponent(Assets.assetManager.get("skin/skin.json", Skin::class.java).getSprite(treeName)))
        val road = ((Gdx.graphics.width - roadSize) / 2.0f - (treeSize / 3.0f)).toInt()..((Gdx.graphics.width + roadSize) / 2.0f + (treeSize / 3.0f)).toInt()
        val x = ((treeSize / 3.0f).toInt()..(Gdx.graphics.width - treeSize / 3.0f).toInt()).filter { !road.contains(it) }.random()
        val y = Gdx.graphics.height + treeSize
        val tC = TransformComponent(x.toFloat(), y, treeSize, treeSize).apply { angle =  (0..360).random().toFloat()}
        tree.add(tC)
        tree.add(MoveComponent(0.0f, -speed))
        engine.addEntity(tree)
    }

    private fun spawnCar() {
        val car = Entity()
        val carName = "car_${(1..10).random()}"
        car.add(DrawComponent(Assets.assetManager.get("skin/skin.json", Skin::class.java).getSprite(carName)))
        car.add(CarComponent())

        val tC: TransformComponent
        val mC: MoveComponent

        val down = Random.nextBoolean()
        if(down) {
            val x = roadLines.slice(0..1).random()
            tC = TransformComponent(x, Gdx.graphics.height + carWidth * 2, carWidth, carWidth * 2)
            tC.angle = 180.0f
            mC = MoveComponent(0.0f,-speed * 1.5f)
        } else {
            val x = roadLines.slice(2..3).random()
            tC = TransformComponent(x, - carWidth * 2, carWidth, carWidth * 2)
            mC = MoveComponent(0.0f,speed * 1.5f)
        }
        car.add(tC)
        car.add(mC)
        engine.addEntity(car)
    }

    private fun spawnBonus() {
        val bonus = Entity()
        val bC = BonusComponent(Bonuses.values().random())
        bonus.add(bC)
        bonus.add(DrawComponent(Assets.assetManager.get("skin/skin.json", Skin::class.java).getSprite(bC.bonus.spriteName)))
        val tC = TransformComponent(roadLines.random(), Gdx.graphics.height + carWidth, 0.8f, carWidth, carWidth)
        bonus.add(tC)
        bonus.add(MoveComponent(0.0f,-speed))
        engine.addEntity(bonus)
    }


}