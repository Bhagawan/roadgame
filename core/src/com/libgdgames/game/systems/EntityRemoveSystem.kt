package com.libgdgames.game.systems

import com.badlogic.ashley.core.ComponentMapper
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.Family
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx
import com.libgdgames.game.components.MoveComponent
import com.libgdgames.game.components.TransformComponent

class EntityRemoveSystem(priority: Int): IteratingSystem(Family.all(MoveComponent::class.java, TransformComponent::class.java).get(), priority) {
    private val tM = ComponentMapper.getFor(TransformComponent::class.java)

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        val tC = tM.get(entity)
        val xBorders = -100..Gdx.graphics.width + 100
        val yBorders = -500..Gdx.graphics.height + 500
        if(!xBorders.contains(tC.pos.x.toInt()) || ! yBorders.contains(tC.pos.y.toInt())) engine.removeEntity(entity)
    }

}