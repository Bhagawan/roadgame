package com.libgdgames.game.systems

import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Dialog
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable
import com.badlogic.gdx.utils.Align
import com.libgdgames.game.RoadGame
import com.libgdgames.game.assets.Assets
import com.libgdgames.game.data.Record
import com.libgdgames.game.util.Records
import java.util.Date

class UISystem(priority: Int): EntitySystem(priority) {
    private val stage = Stage()
    private val table: Table = Table()
    private var score = 0

    private val scoreLabel = Label("$score", Assets.assetManager.get("skin/skin.json", Skin::class.java), "score").apply { setAlignment(Align.center) }
    private val pausePopup = createPausePopup()

    private var mInterface: UIInterface? = null
    private var onPause = false
    private var timeD = 0.0f

    private var explosionTimer: Int? = null

    init {
        table.setFillParent(true)
        table.pad(10.0f)
        stage.addActor(table)
        createUI()
    }

    override fun update(deltaTime: Float) {
        stage.act(deltaTime)
        stage.draw()
        timeD += deltaTime
        if(timeD > RoadGame.PERFECT_DT) {
            timeD = 0.0f
            if(!onPause) {
                score++
                scoreLabel.setText("$score")
            }
           if(explosionTimer != null) {
               explosionTimer = explosionTimer!! -1
               if(explosionTimer!! <= 0) {
                   explosionTimer = null
                   createEndPopup().show(stage)
               }
           }
        }
    }

    private fun createUI() {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)

        val menuButton = ImageButton(SpriteDrawable(skin.getSprite("btn_menu").apply { setSize(Gdx.graphics.width / 10.0f, Gdx.graphics.width / 10.0f) })).apply {
            addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    pause()
                }
            })
            val size = Gdx.graphics.width / 10.0f
            setSize(size, size)
        }
        table.top().left().add(menuButton)

        table.add(scoreLabel).expandX().height(Gdx.graphics.width / 10.0f + 10).center()

        table.add().size(Gdx.graphics.width / 10.0f)
        table.row()
    }

    private fun createPausePopup(): Dialog {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val pausePopup = Dialog("", skin)
        pausePopup.pad(5.0f)
        pausePopup.width = Gdx.graphics.width / 2.0f
        pausePopup.buttonTable.width = Gdx.graphics.width / 2.0f
        pausePopup.contentTable.width = Gdx.graphics.width / 2.0f

        val btnExit = ImageButton(skin.getDrawable("btn_exit")).apply {
            addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    mInterface?.exit()
                    pausePopup.hide()
                }
            })
        }
        val btnContinue = ImageButton(skin.getDrawable("btn_continue")).apply {
            addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    resume()
                    pausePopup.hide()
                }
            })
        }
        pausePopup.contentTable.add(Label("Pause", skin, "Font_60", "Red").apply { setAlignment(Align.center) }).width(Gdx.graphics.width / 2.0f)
        pausePopup.contentTable.row()
        pausePopup.buttonTable.pad(20.0f)
        pausePopup.buttonTable
        pausePopup.buttonTable.add(btnExit).pad(5.0f).padRight(10.0f)
        pausePopup.buttonTable.add(btnContinue).pad(5.0f).padLeft(10.0f)
        return pausePopup
    }

    private fun createEndPopup(): Dialog {
        val skin = Assets.assetManager.get("skin/skin.json", Skin::class.java)
        val endPopup = Dialog("", skin)
        endPopup.pad(5.0f)
        endPopup.width = Gdx.graphics.width / 2.0f

        val btnExit = ImageButton(skin.getDrawable("btn_exit")).apply {
            addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    mInterface?.exit()
                    endPopup.hide()
                }
            })
        }
        val btnRestart = ImageButton(skin.getDrawable("btn_restart")).apply {
            addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    restart()
                    endPopup.hide()
                }
            })
        }

        endPopup.contentTable.add(Label("End", skin, "Font_60", "Red").apply { setAlignment(Align.center) }).width(Gdx.graphics.width / 2.0f)
        endPopup.contentTable.row()
        endPopup.contentTable.add(Label("You score:", skin)).padTop(30.0f)
        endPopup.contentTable.row()
        endPopup.contentTable.add(Label("$score", skin))
        endPopup.contentTable.row().height(30.0f)

        endPopup.buttonTable.pad(20.0f)
        endPopup.buttonTable.add(btnExit).pad(5.0f)
        endPopup.buttonTable.add(btnRestart).pad(5.0f)
        return endPopup
    }

    private fun pause() {
        mInterface?.pause()
        pausePopup.show(stage)
        onPause = true
    }

    private fun resume() {
        mInterface?.resume()
        onPause = false
    }

    private fun restart() {
        mInterface?.restart()
        score = 0
        scoreLabel.setText("0")
        onPause = false
    }

    fun setInterface(i: UIInterface) {
        mInterface = i
    }

    fun end() {
        explosionTimer = 30
        onPause = true
        saveRecord()
    }

    fun getStage(): Stage = stage

    private fun saveRecord() {
        Records.saveRecord(Record(score, Date()))
    }

    interface UIInterface {
        fun pause()
        fun resume()
        fun restart()
        fun exit()
    }
}