package com.libgdgames.game.systems

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.libgdgames.game.assets.Assets
import com.libgdgames.game.components.DrawComponent
import com.libgdgames.game.components.EffectComponent
import com.libgdgames.game.components.PlayerComponent
import com.libgdgames.game.components.TransformComponent
import com.libgdgames.game.data.Bonuses
import kotlin.math.absoluteValue

class ControlSystem(priority: Int): EntitySystem(priority) {
    private var gameOn = true
    private val roadSize: Float = Gdx.graphics.width / 2.0f
    private val carWidth = roadSize * (56.0f / 300.0f)
    private val roadLines = listOf(
        (Gdx.graphics.width - roadSize) / 2.0f + roadSize * (19.0f / 300.0f),
        (Gdx.graphics.width - roadSize) / 2.0f + roadSize * (83.0f / 300.0f),
        (Gdx.graphics.width - roadSize) / 2.0f + roadSize * (164.0f / 300.0f),
        (Gdx.graphics.width - roadSize) / 2.0f + roadSize * (227.0f / 300.0f)
    )

    val playerTC = TransformComponent(roadLines[2],0.0f,carWidth,carWidth * 2)

    private var playerTargetLine: Int? = null
    private var playerTargetY: Float? = null
    private val playerSpeed = carWidth / 10.0f

    private var controlInterface: ControlInterface? = null

    private var difficultyTimer = 0


    override fun addedToEngine(engine: Engine?) {
        val player  = Entity()
        player.add(PlayerComponent())
        player.add(DrawComponent(Assets.assetManager.get("skin/skin.json", Skin::class.java).getSprite("car_1")))
        playerTC.pos.y = Gdx.graphics.height / 2.0f
        player.add(playerTC)
        engine?.addEntity(player)

    }

    override fun update(deltaTime: Float) {
        if(gameOn) {
            playerTargetLine?.let {
                if(playerTC.pos.x < roadLines[it]) playerTC.pos.x += carWidth / 10
                else playerTC.pos.x -= carWidth / 10
                if((playerTC.pos.x - roadLines[it]).absoluteValue < carWidth / 10) {
                    playerTC.pos.x = roadLines[it]
                    playerTargetLine = null
                }
            }
            playerTargetY?.let {
                if((playerTC.pos.y - it).absoluteValue <= playerSpeed) {
                    playerTC.pos.y = it
                    playerTargetY = null
                } else if(playerTC.pos.y < it) playerTC.pos.y += playerSpeed
                else playerTC.pos.y -= playerSpeed
                if(playerTC.pos.y < 30) explosion(playerTC.pos.x + playerTC.width / 2.0f, playerTC.pos.y)
                else if(playerTC.pos.y > Gdx.graphics.height - playerTC.height - 30) explosion(playerTC.pos.x + playerTC.width / 2.0f, playerTC.pos.y + playerTC.height)
            }

            if(difficultyTimer++ > 30) {
                difficultyTimer = 0
                controlInterface?.increaseSpeed()
            }
        }
    }

    fun touch(x: Float) {
        if(x > playerTC.pos.x && playerTC.pos.x < roadLines[3]) {
            for(line in roadLines.withIndex()) {
                if(playerTC.pos.x == line.value ) {
                    playerTargetLine = line.index + 1
                    break
                }
            }
        } else if(x < playerTC.pos.x && playerTC.pos.x > roadLines[0]) {
            for(line in roadLines.withIndex()) {
                if(playerTC.pos.x == line.value ) {
                    playerTargetLine = line.index - 1
                    break
                }
            }
        }
    }

    fun pauseGame() {
        gameOn = false
        for(system in engine.systems) {
            if(system is BackgroundSystem) system.setProcessing(false)
            if(system is MovementSystem) system.setProcessing(false)
            if(system is SpawnSystem) system.setProcessing(false)
            if(system is CollisionSystem) system.setProcessing(false)
        }
    }

    fun resumeGame() {
        gameOn = true
        for(system in engine.systems) {
            if(system is BackgroundSystem) system.setProcessing(true)
            if(system is MovementSystem) system.setProcessing(true)
            if(system is CollisionSystem) system.setProcessing(true)
            if(system is SpawnSystem) system.setProcessing(true)
        }
    }

    fun restartGame() {
        playerTC.pos.y = Gdx.graphics.height / 2.0f
        playerTargetLine = null
        playerTargetY = null
        playerTC.pos.x = roadLines[2]
        resumeGame()
    }

    fun explosion(x: Float, y: Float) {
        val explosion = Entity()
        explosion.add(EffectComponent(x, y, Assets.explosionPool.obtain()))
        engine.addEntity(explosion)
        pauseGame()
        controlInterface?.endGameMenu()
    }

    fun applyBonus(bonus: Bonuses) {
        playerTargetY = when(bonus) {
            Bonuses.GAS -> {
                (playerTC.pos.y + playerTC.height * 1.5f).coerceAtMost(Gdx.graphics.height - playerTC.height)
            }

            Bonuses.OIL -> {
                (playerTC.pos.y - playerTC.height * 1.5f).coerceAtLeast(0.0f)
            }
        }
    }

    fun setInterface(i: ControlInterface) {
        controlInterface = i
    }

    interface ControlInterface {
        fun endGameMenu()
        fun increaseSpeed()
    }
}