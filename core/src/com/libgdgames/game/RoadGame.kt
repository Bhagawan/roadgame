package com.libgdgames.game

import com.badlogic.gdx.Game
import com.libgdgames.game.assets.Assets
import com.libgdgames.game.screens.MenuScreen

class RoadGame : Game() {

    companion object {
        const val PERFECT_DT: Float = (16.67 * 2 / 1000).toFloat()
    }

    override fun create () {
        Assets.init()
        setScreen(MenuScreen(this))
    }

    override fun dispose () {
        Assets.dispose()
    }
}