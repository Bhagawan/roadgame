package com.libgdgames.game.util

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Json
import com.libgdgames.game.data.Record

class Records {
    companion object {

        fun saveRecord(record: Record) {
            val pref = Gdx.app.getPreferences("roadGame")
            val j = Json()
            var savedRecords = j.fromJson(Array<Record>::class.java, pref.getString("records", "[]"))
            savedRecords = savedRecords.plusElement(record)
            savedRecords.sortByDescending { it.record }
            pref.putString("records", j.toJson(savedRecords))
            pref.flush()
        }

        fun getRecords(): Array<Record> {
            val prefs = Gdx.app.getPreferences("roadGame")
            val r = prefs.getString("records", "[]")
            return Json().fromJson(Array<Record>::class.java, r)
        }
    }
}